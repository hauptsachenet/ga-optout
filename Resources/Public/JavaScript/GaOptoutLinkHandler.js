/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Module: Hn/HnPortfolio/Recordlist/SpecLinkHandler
 * Spec link interaction
 */
define(['jquery', 'TYPO3/CMS/Recordlist/LinkBrowser'], function($, LinkBrowser) {
	'use strict';

	/**
	 *
	 * @type {{currentLink: string}}
	 * @exports TYPO3/CMS/Recordlist/SpecLinkHandler
	 */
	var GaOptoutLinkHandler = {
		currentLink: ''
	};

	/**
	 *
	 * @param {Event} event
	 */
    GaOptoutLinkHandler.linkSpec = function(event) {
		event.preventDefault();
		LinkBrowser.finalizeFunction('t3://ga-optout?success=' + $('#lgaoptout-success').val() + '&error=' + $('#lgaoptout-error').val());
	};

	/**
	 *
	 * @param {Event} event
	 */
    GaOptoutLinkHandler.linkCurrent = function(event) {
		event.preventDefault();
		LinkBrowser.finalizeFunction(GaOptoutLinkHandler.currentLink);
	};

	$(function() {
        GaOptoutLinkHandler.currentLink = $('body').data('currentLink');
        $('#lgaoptoutform').on('submit', GaOptoutLinkHandler.linkSpec)
	});

	return GaOptoutLinkHandler;
});
