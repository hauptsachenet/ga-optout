<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Google Analytics Optout Link Handler',
    'description' => 'Adds a linkhandler to TYPO3.',
    'category' => 'fe',
    'version' => '0.0.1',
    'state' => 'beta',
    'uploadfolder' => false,
    'author' => 'Florian Grell, Marco Pfeiffer',
    'author_email' => 'florian@hauptsache.net, marco@hauptsache.net',
    'author_company' => 'hauptsache.net GmbH',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-10.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'createDirs' => null,
    'clearcacheonload' => false,
];
