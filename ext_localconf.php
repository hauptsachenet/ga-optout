<?php

call_user_func(function ($extkey) {

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['linkHandler']['ga-optout'] = \Hn\GaOptout\LinkHandling\GaOptoutLinkHandler::class;
    $GLOBALS['TYPO3_CONF_VARS']['FE']['typolinkBuilder']['ga-optout'] = \Hn\GaOptout\LinkHandling\GaOptoutLinkBuilder::class;

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig("<INCLUDE_TYPOSCRIPT: source=\"FILE:EXT:ga_optout/Configuration/TSconfig/Page/tsconfig.txt\">");

}, 'ga_optout');