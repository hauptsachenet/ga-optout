<?php

namespace Hn\GaOptout\LinkHandling;


use TYPO3\CMS\Frontend\Typolink\AbstractTypolinkBuilder;
use TYPO3\CMS\Frontend\Typolink\UnableToLinkException;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class GaOptoutLinkBuilder extends AbstractTypolinkBuilder
{
    /**
     * Should be implemented by all subclasses to return an array with three parts:
     * - URL
     * - Link Text (can be modified)
     * - Target (can be modified)
     *
     * @param array $linkDetails parsed link details by the LinkService
     * @param string $linkText the link text
     * @param string $target the target to point to
     * @param array $conf the TypoLink configuration array
     * @return array an array with three parts (URL, Link Text, Target)
     * @throws UnableToLinkException
     */
    public function build(array &$linkDetails, string $linkText, string $target, array $conf): array
    {
        $tagBuilder = new TagBuilder('a');
        $tagBuilder->setContent($linkText);
        $tagBuilder->forceClosingTag(true);

        $tagBuilder->addAttribute('onclick', 'gaOptout(); alert(' . json_encode($linkDetails['success']) . '); return false');
        $tagBuilder->addAttribute('href', 'Javascript: alert(' . json_encode($linkDetails['error']) . ')');

        throw new UnableToLinkException('', 0, null, $tagBuilder->render());
    }
}