<?php

namespace Hn\GaOptout\LinkHandling;

use TYPO3\CMS\Core\LinkHandling\LinkHandlingInterface;

class GaOptoutLinkHandler implements LinkHandlingInterface
{
    /**
     * Returns a string interpretation of the link href query from objects, something like
     *
     *  - t3://page?uid=23&my=value#cool
     *  - https://www.typo3.org/
     *  - t3://file?uid=13
     *  - t3://folder?storage=2&identifier=/my/folder/
     *  - mailto:mac@safe.com
     *
     * array of data -> string
     *
     * @param array $parameters
     * @return string
     */
    public function asString(array $parameters): string
    {
        return 't3://ga-optout?' . http_build_query($parameters);
    }

    /**
     * Returns a array with data interpretation of the link href from parsed query parameters of urn
     * representation.
     *
     * array of strings -> array of data
     *
     * @param array $data
     * @return array
     */
    public function resolveHandlerData(array $data): array
    {
        return [
            'success' => !empty($data['success']) ? $data['success'] : "Google Analytics disabled",
            'error' => !empty($data['error']) ? $data['error'] : "Failed to disable Google Analytics",
        ];
    }
}