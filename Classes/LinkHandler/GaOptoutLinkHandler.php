<?php
/**
 * Created by PhpStorm.
 * User: floriangrell
 * Date: 10.04.18
 * Time: 15:44
 */

namespace Hn\GaOptout\LinkHandler;


use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Recordlist\Controller\AbstractLinkBrowserController;
use TYPO3\CMS\Recordlist\LinkHandler\LinkHandlerInterface;

class GaOptoutLinkHandler implements LinkHandlerInterface
{

    protected $successMessage;
    protected $errorMessage;

    /**
     * Checks if this is the handler for the given link
     *
     * The handler may store this information locally for later usage.
     *
     * @param array $linkParts Link parts as returned from TypoLinkCodecService
     *
     * @return bool
     */
    public function canHandleLink(array $linkParts)
    {
        if ($linkParts['type'] !== 'ga-optout') {
            return false;
        }

        $this->successMessage = $linkParts['url']['success'];
        $this->errorMessage = $linkParts['url']['error'];
        return true;
    }



    /**
     * Render the link handler
     *
     * @param ServerRequestInterface $request
     *
     * @return string
     */
    public function render(ServerRequestInterface $request)
    {

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $jsPathAndFilename = GeneralUtility::getFileAbsFileName('EXT:ga_optout/Resources/Public/JavaScript/GaOptoutLinkHandler.js');
        $pageRenderer->loadRequireJsModule(PathUtility::getAbsoluteWebPath($jsPathAndFilename));


        $path = GeneralUtility::getFileAbsFileName('EXT:ga_optout/Resources/Private/Templates/LinkBrowser/GaOptoutLinkHandler.html');
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->setTemplatePathAndFilename($path);
        $view->assignMultiple([
            'success' => $this->successMessage,
            'error' => $this->errorMessage,
        ]);

        return $view->render();
    }


    /**
     * Return TRUE if the handler supports to update a link.
     *
     * This is useful for file or page links, when only attributes are changed.
     *
     * @return bool
     */
    public function isUpdateSupported()
    {
        // no update
        return false;
    }

    /**
     * @return string[] Array of body-tag attributes
     */
    public function getBodyTagAttributes()
    {
        // no body tag attributes
        return [];
    }

    /**
     * @return array
     */
    public function getLinkAttributes()
    {
        return ['title'];
    }

    /**
     * @param string[] $fieldDefinitions Array of link attribute field definitions
     * @return string[]
     */
    public function modifyLinkAttributes(array $fieldDefinitions)
    {
        return $fieldDefinitions;
    }

    /**
     * Initialize the handler
     *
     * @param AbstractLinkBrowserController $linkBrowser
     * @param string $identifier
     * @param array $configuration Page TSconfig
     */
    public function initialize(AbstractLinkBrowserController $linkBrowser, $identifier, array $configuration)
    {
        // TODO: Implement initialize() method.
    }

    /**
     * Format the current link for HTML output
     *
     * @return string
     */
    public function formatCurrentUrl()
    {
        return 'Google Analytics Optout Link';
    }
}